"use strict";

function longRepeat(line) {
    // length the longest substring that consists of the same char

    let count = 1;
    let max = 1;
    if (!line) {
        return 0;
    }
    for (let i = 1; i <= line.length; i++) {
        if (line[i-1] === line[i]) {
            count += 1;
        } else if (count > max) {
            max = count;
            count = 1;
        } else {
            count = 1;
        }
    }
    return max;
}

let assert = require("assert");

if (!global.is_checking) {
    assert.equal(longRepeat("sdsffffse"), 4, `First: ${longRepeat("sdsffffse")}`);
    assert.equal(longRepeat("ddvvrwwwrggg"), 3, `Second: ${longRepeat("ddvvrwwwrggg")}`);
    assert.equal(longRepeat(""), 0, "Third");
    assert.equal(longRepeat("aa"), 2, "Fourth");
    console.log('"Run" is good. How is "Check"?');
}

// console.log(longRepeat("ff"));
